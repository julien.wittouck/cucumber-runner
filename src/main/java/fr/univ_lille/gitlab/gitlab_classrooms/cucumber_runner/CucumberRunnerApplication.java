package fr.univ_lille.gitlab.gitlab_classrooms.cucumber_runner;

import io.cucumber.core.cli.Main;

public class CucumberRunnerApplication {

	public static void main(String[] args) {
		// run cucumber !
		Main.main(args);
	}

}
