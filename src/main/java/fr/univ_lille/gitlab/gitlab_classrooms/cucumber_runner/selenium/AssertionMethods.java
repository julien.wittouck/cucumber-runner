package fr.univ_lille.gitlab.gitlab_classrooms.cucumber_runner.selenium;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class AssertionMethods extends SelectElementByType implements BaseTest {
    //This file contains assertion methods which are called from predefinedStepDefinitions

    //SelectElementByType eleType= new SelectElementByType();
    private WebElement element = null;

    /**
     * Method to get page title
     *
     * @return String
     */
    public String getPageTitle() {
        return driver.getTitle();
    }

    /**
     * Method to verify page title
     *
     * @param title    : String : expected title
     * @param testCase : Boolean : test case [true or false]
     */
    public void checkTitle(String title, boolean testCase) {
        String pageTitle = getPageTitle();

        if (testCase) {
            assertThat(pageTitle)
                    .as("check page title")
                    .isEqualTo(title);
        } else {
            assertThat(pageTitle)
                    .as("check page title")
                    .isNotEqualTo(title);
        }
    }

    /**
     * Method to verify partial page title
     *
     * @param partialTitle : String : partial title string
     * @param testCase     : Boolean : test case [true or false]
     */
    public void checkPartialTitle(String partialTitle, boolean testCase) {
        String pageTitle = getPageTitle();
        if (testCase) {
            if (!pageTitle.contains(partialTitle))
                fail("Partial Page Title Not Present, Actual Page Title : " + pageTitle);
        } else {
            if (pageTitle.contains(partialTitle))
                fail("Partial Page Title Present, Actual Page Title : " + pageTitle);
        }
    }

    /**
     * Method to get element text
     *
     * @param accessType : String : Locator type (id, name, class, xpath, css)
     * @param accessName : String : Locator value
     * @return String
     */
    public String getElementText(String accessType, String accessName) {
        element = wait.until(ExpectedConditions.presenceOfElementLocated(getelementbytype(accessType, accessName)));
        return element.getText();

    }

    /**
     * Method to check element text
     *
     * @param accessType  : String : Locator type (id, name, class, xpath, css)
     * @param actualValue : String : Expected element text
     * @param accessName  : String : Locator value
     * @param testCase    : Boolean : test case [true or false]
     */
    public void checkElementText(String accessType, String actualValue, String accessName, boolean testCase) {
        String elementText = getElementText(accessType, accessName);

        if (testCase) {
            assertThat(elementText).isEqualTo(actualValue);
        } else {
            assertThat(elementText).isNotEqualTo(actualValue);
        }
    }

    /**
     * Method to check partial element text
     *
     * @param accessType  : String : Locator type (id, name, class, xpath, css)
     * @param actualValue : String : Expected element text
     * @param accessName  : String : Locator value
     * @param testCase    : Boolean : test case [true or false]
     */
    public void checkElementPartialText(String accessType, String actualValue, String accessName, boolean testCase) {
        String elementText = getElementText(accessType, accessName);

        if (testCase) {
            if (!elementText.contains(actualValue))
                fail("Text Not Matched");
        } else {
            if (elementText.contains(actualValue))
                fail("Text Matched");
        }
    }

    /**
     * Method to return element status - enabled?
     *
     * @param accessType : String : Locator type (id, name, class, xpath, css)
     * @param accessName : String : Locator value
     * @return Boolean
     */
    public boolean isElementEnabled(String accessType, String accessName) {
        element = wait.until(ExpectedConditions.presenceOfElementLocated(getelementbytype(accessType, accessName)));
        return element.isEnabled();
    }

    /**
     * Element enabled checking
     *
     * @param accessType : String : Locator type (id, name, class, xpath, css)
     * @param accessName : String : Locator value
     * @param testCase   : Boolean : test case [true or false]
     */
    public void checkElementEnable(String accessType, String accessName, boolean testCase) {
        boolean result = isElementEnabled(accessType, accessName);
        if (testCase) {
            if (!result)
                fail("Element Not Enabled");
        } else {
            if (result)
                fail("Element Enabled");
        }
    }

    /**
     * method to get attribute value
     *
     * @param accessType    : String : Locator type (id, name, class, xpath, css)
     * @param accessName    : String : Locator value
     * @param attributeName : String : attribute name
     * @return String
     */
    public String getElementAttribute(String accessType, String accessName, String attributeName) {
        element = wait.until(ExpectedConditions.presenceOfElementLocated(getelementbytype(accessType, accessName)));
        return element.getAttribute(attributeName);
    }

    /**
     * method to check attribute value
     *
     * @param accessType     : String : Locator type (id, name, class, xpath, css)
     * @param attributeName  : String : attribute name
     * @param attributeValue : String : attribute value
     * @param accessName     : String : Locator value
     * @param testCase       : Boolean : test case [true or false]
     */
    public void checkElementAttribute(String accessType, String attributeName, String attributeValue, String accessName, boolean testCase) {
        String attrVal = getElementAttribute(accessType, accessName, attributeName);
        if (testCase) {
            if (!attrVal.equals(attributeValue))
                fail("Attribute Value Not Matched");
        } else {
            if (attrVal.equals(attributeValue))
                fail("Attribute Value Matched");
        }
    }

    /**
     * method to get element status - displayed?
     *
     * @param accessType : String : Locator type (id, name, class, xpath, css)
     * @param accessName : String : Locator value
     * @return Boolean
     */
    public boolean isElementDisplayed(String accessType, String accessName) {
        element = wait.until(ExpectedConditions.presenceOfElementLocated(getelementbytype(accessType, accessName)));
        return element.isDisplayed();
    }

    /**
     * method to check element presence
     *
     * @param accessType : String : Locator type (id, name, class, xpath, css)
     * @param accessName : String : Locator value
     * @param testCase   : Boolean : test case [true or false]
     */
    public void checkElementPresence(String accessType, String accessName, boolean testCase) {
        if (testCase) {
            try {
                if (!isElementDisplayed(accessType, accessName))
                    fail("Element with %s '%s' not present.".formatted(accessType, accessName));
            } catch (TimeoutException e) {
                fail("Element with %s '%s' not present.".formatted(accessType, accessName));
            }

        } else {
            try {
                if (isElementDisplayed(accessType, accessName))
                    fail("Element with %s '%s' present.".formatted(accessType, accessName));
            } catch (Exception e) {
                if (e.getMessage().equals("Present")) //only raise if it present
                    fail("Element with %s '%s' present.".formatted(accessType, accessName));
            }
        }
    }

    /**
     * method to assert checkbox check/uncheck
     *
     * @param accessType      : String : Locator type (id, name, class, xpath, css)
     * @param accessName      : String : Locator value
     * @param shouldBeChecked : Boolean : test case [true or false]
     */
    public void isCheckboxChecked(String accessType, String accessName, boolean shouldBeChecked) {
        WebElement checkbox = wait.until(ExpectedConditions.presenceOfElementLocated(getelementbytype(accessType, accessName)));
        if ((!checkbox.isSelected()) && shouldBeChecked)
            fail("Checkbox is not checked");
        else if (checkbox.isSelected() && !shouldBeChecked)
            fail("Checkbox is checked");
    }

    /**
     * method to assert radio button selected/unselected
     *
     * @param accessType      : String : Locator type (id, name, class, xpath, css)
     * @param accessName      : String : Locator value
     * @param shouldBeChecked : Boolean : test case [true or false]
     */
    public void isRadioButtonSelected(String accessType, String accessName, boolean shouldBeSelected) {
        WebElement radioButton = wait.until(ExpectedConditions.presenceOfElementLocated(getelementbytype(accessType, accessName)));
        if ((!radioButton.isSelected()) && shouldBeSelected)
            fail("Radio Button not selected");
        else if (radioButton.isSelected() && !shouldBeSelected)
            fail("Radio Button is selected");
    }

    //method to assert option from radio button group is selected/unselected
    public void isOptionFromRadioButtonGroupSelected(String accessType, String by, String option, String accessName, boolean shouldBeSelected) {
        List<WebElement> radioButtonGroup = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(getelementbytype(accessType, accessName)));

        for (WebElement rb : radioButtonGroup) {
            if (by.equals("value")) {
                if (rb.getAttribute("value").equals(option)) {
                    if ((!rb.isSelected()) && shouldBeSelected)
                        fail("Radio Button not selected");
                    else if (rb.isSelected() && !shouldBeSelected)
                        fail("Radio Button is selected");
                }
            } else if (rb.getText().equals(option)) {
                if ((!rb.isSelected()) && shouldBeSelected)
                    fail("Radio Button not selected");
                else if (rb.isSelected() && !shouldBeSelected)
                    fail("Radio Button is selected");
            }
        }
    }

    /**
     * method to get javascript pop-up alert text
     *
     * @return String
     */
    public String getAlertText() {
        return driver.switchTo().alert().getText();
    }

    /**
     * method to check javascript pop-up alert text
     *
     * @param text : String : Text to verify in Alert
     */
    public void checkAlertText(String text) {
        if (!getAlertText().equals(text))
            fail("Text on alert pop up not matched");
    }

    /**
     * Method to verify if the particular option is Selected from Dropdown
     *
     * @param accessType       : String : Locator type (id, name, class, xpath, css)
     * @param by               : String : Select element from dropdown by text or value
     * @param option           : String : Element to select from dropdown
     * @param accessName       : String : Locator value
     * @param shouldBeSelected : Boolean : test case [true or false]
     */
    public void isOptionFromDropdownSelected(String accessType, String by, String option, String accessName, boolean shouldBeSelected) {
        Select selectList = null;
        WebElement dropdown = wait.until(ExpectedConditions.presenceOfElementLocated(getelementbytype(accessType, accessName)));
        selectList = new Select(dropdown);

        String actualValue = "";
        if (by.equals("text"))
            actualValue = selectList.getFirstSelectedOption().getText();
        else
            actualValue = selectList.getFirstSelectedOption().getAttribute("value");

        if ((!actualValue.equals(option)) && (shouldBeSelected))
            fail("Option Not Selected From Dropwdown");
        else if ((actualValue.equals(option)) && (!shouldBeSelected))
            fail("Option Selected From Dropwdown");
    }
}
