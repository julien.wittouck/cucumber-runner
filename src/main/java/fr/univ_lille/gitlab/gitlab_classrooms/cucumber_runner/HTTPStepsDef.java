package fr.univ_lille.gitlab.gitlab_classrooms.cucumber_runner;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class HTTPStepsDef {

    private HttpResponse<String> response;

    private int port = 8080;
    private String host = "localhost";

    @Given("L'application écoute sur le port {int}")
    @Given("The server listens on port {int}")
    public void serverListensOnPort(int port) throws IOException {
        this.port = port;
        try(var socket = new Socket("localhost", port)){
            assertThat(socket.isConnected()).isTrue();
        }
        catch(ConnectException e){
            fail("Could not connect to port " + port);
        }
    }

    @When("La requête HTTP GET {string} est envoyée")
    @When("HTTP request GET {string} is send")
    public void httpGet(String uri) throws IOException, InterruptedException {
        var request = HttpRequest.newBuilder()
                .uri(URI.create("http://" + host + ":" + port + uri))
                .GET()
                .build();

        var client = HttpClient.newHttpClient();
        this.response = client.send(request, HttpResponse.BodyHandlers.ofString());
    }

    @When("HTTP request POST {string} is send with form parameters {string}")
    public void httpPostWithParameters(String uri, String parameters) throws IOException, InterruptedException {
        var request = HttpRequest.newBuilder()
                .uri(URI.create("http://" + host + ":" + port + uri))
                .header("Content-Type", "application/x-www-form-urlencoded")
                .POST(HttpRequest.BodyPublishers.ofString(parameters))
                .build();

        var client = HttpClient.newHttpClient();
        this.response = client.send(request, HttpResponse.BodyHandlers.ofString());
    }

    @Then("La réponse a un code HTTP {int}")
    @Then("HTTP response code is {int}")
    public void responseCodeIs(int code) {
        assertThat(response.statusCode()).isEqualTo(code);
    }

    @Then("La réponse a un header HTTP {string}")
    @Then("HTTP response has header matching {string}")
    public void responseHeadersContains(String header) {
        var headerParts = header.split(": ");
        var headerKey = headerParts[0];
        var headerValue = headerParts[1];
        assertThat(response.headers().allValues(headerKey)).anyMatch(it -> it.contains(headerValue));
    }

    @Then("La réponse a un body {string}")
    @Then("HTTP response as body equal to {string}")
    public void responseBodyEqualTo(String body) {
        assertThat(response.body()).isEqualTo(body);
    }
}
